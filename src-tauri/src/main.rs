// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]
// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
use std::error::Error;
use mongodb::{Client, Collection, options::ClientOptions, bson::{Document}, bson::doc, bson::oid::ObjectId};
use serde_json::Value;
use serde::{Deserialize, Serialize};
use futures::TryStreamExt;
use tauri::api::dialog::blocking::message;

const MONGODB_CONNECTION_STRING: &str = "mongodb://localhost:27030"; 

#[derive(Debug, Deserialize, Serialize)]
struct Message {
    id: String,
    message: String,
    timestamp: String,
}

#[tauri::command]
fn greet(name: &str) -> String {
    format!("Hello, {}! You've been greeted from Rust!", name)
}

#[tauri::command]
async fn save_message(invoke_message: String) {
//   println!("I was invoked from JS, with this message: {}", invoke_message);
    if invoke_message.len() > 2 {
        let json_string: Result<Message, serde_json::Error> = serde_json::from_str(&invoke_message);
        match json_string {
            Ok(my_struct) => {
                // println!("{}", my_struct.message);
                println!("{:?}", my_struct);
                save_message_to_collection(&my_struct).await;
            },
            Err(err) => {
                eprintln!("{}", err);
            }
        }
    }
}

#[tauri::command]
async fn get_message() -> Vec<Message> {
    let messages = unsafe {
        db_get_message().await.expect("error fetching message.")
    };

    messages
}

#[tauri::command]
async fn remove_message(invoke_message: String) {
//   println!("I was invoked from JS, with this message: {}", invoke_message);
    db_remove_message_by_id(&invoke_message).await;
}

async fn db_remove_message_by_id(id: &str) ->Result<(), Box<dyn Error>> {
    println!("get id :{}", id);
    let client_options = ClientOptions::parse(MONGODB_CONNECTION_STRING).await?;
    let client = Client::with_options(client_options)?;
    let db = client.database("my-note");
    let coll: Collection<Document> = db.collection("message");
    let object_id_result = ObjectId::parse_str(id)?;
    let filter = doc! {
        "_id": object_id_result,
    };

    coll.delete_one(filter, None).await?;
    Ok(())
}

async fn db_get_message() -> Result<Vec<Message>, Box<dyn Error>> {
    let client_options = ClientOptions::parse(MONGODB_CONNECTION_STRING).await?;
    let client = Client::with_options(client_options)?;
    let db = client.database("my-note");
    let coll: Collection<Document> = db.collection("message");
    let mut cursor = coll.find(None, None).await?;
    let mut messages:Vec<Message> = Vec::new();
    while let Some(result) = cursor.try_next().await? {
        // println!("{:?}", result.get_object_id("_id")?.to_string());
        let message = Message {
            id: result.get_object_id("_id")?.to_string(),
            message: result.get_str("message")?.to_string(),
            timestamp: result.get_str("timestamp")?.to_string()
        };

        messages.push(message);
    }
   
    Ok(messages)
}

async fn save_message_to_collection(data: &Message) -> Result<(), Box<dyn Error>> {
    let client_options = ClientOptions::parse(MONGODB_CONNECTION_STRING).await?;
    let client = Client::with_options(client_options)?;
    let db = client.database("my-note");
    let coll: Collection<Document> = db.collection("message");
    let document = doc! {
        "message": data.message.clone(),
        "timestamp": data.timestamp.clone(),
    };
    let result = coll.insert_one(document, None).await?;
    println!("{:?}", result);

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>>{

    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![greet, save_message, get_message, remove_message])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");

    Ok(())
}
