import MainEditor from "./editor";
import {useState, useEffect} from 'react';
import Search from "./content-app/search";
import Message from "./content-app/message";
import { IMessage } from "./content-app/message/chat";
import { invoke } from '@tauri-apps/api/tauri';
import './App.css'

function App() {
  const [item, setItem] = useState<Array<IMessage>>([])

  const addItem = (message: IMessage) => {
    let _item = [...item, message];
    setItem(_item)
  }

  const deleteItem = async (id: string) => {
    console.log('invoke')
    invoke('remove_message', { invokeMessage: id})
    const newArray = item.filter((el) => el.id !== id)
    setItem(newArray)
  }

  useEffect(() => {
    const getMesage = async () => {
      try {
        const response: Array<IMessage> = await invoke('get_message')
        setItem(response)
      } catch(e) {
        console.log(e)
      }
    }

    getMesage()

  }, [])


  return (
    <div className="App">
      <div className="content-app">
        <div className="content-search">
          <Search />
        </div>
        <div className="content-mesage">
          <Message items={item} deleteItem={deleteItem}/>
        </div>
      </div>
      <div className="content-editor">
        <MainEditor addItem={addItem} />
      </div>
    </div>
  );
}

export default App;
