import { FC, useEffect, useRef } from 'react';
import { EditorContent, useEditor } from '@tiptap/react';
import StarterKit from '@tiptap/starter-kit';
import './index.css';

export interface IMessage {
  id: string;
  timestamp: Date;
  message: string;
}

function Editor(message: any) {
    const editor = useEditor({
      extensions: [StarterKit],
      content: message.message.message,
      editable: false,
    });
  
    return <EditorContent editor={editor} />;
}
  
const Chat: FC<{ data: Array<IMessage>, deleteItem: Function }> = ({ data, deleteItem }) => {
  const chatRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const editoContentElement = chatRef.current;
    if(editoContentElement !== null) {
      editoContentElement.scrollTop = editoContentElement.scrollHeight;
    }
  },[data])

  const onDeleteItem = (idx: string) => {
    deleteItem(idx)
  }

  return (
    <div className='chat' ref={chatRef}>
      {data.map((el: IMessage) => (
        <div key={el.id} className='chat-content'>
          <div className='title-content'>
            [{el.timestamp.toLocaleString()}]
            <div className='btn-topic'>
              {/* <button>Focus</button> */}
              <button onClick={() => onDeleteItem(el.id)}>Delete</button>
            </div>
          </div>
          <Editor message={el}/>
        </div>
      ))}
    </div>
  );
};

export default Chat;
