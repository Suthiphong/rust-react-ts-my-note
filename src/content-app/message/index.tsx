import {FC} from 'react';
import Chat, { IMessage } from './chat';

const Message: FC<{items: Array<IMessage>, deleteItem: Function}> = ({items, deleteItem}) =>{
    return (
        <Chat data={items} deleteItem={deleteItem}/>
    )
}

export default Message;