import Placeholder from '@tiptap/extension-placeholder'
import {useEditor, EditorContent} from '@tiptap/react'
import StarterKit from '@tiptap/starter-kit'
import "./index.css";
import { useState } from 'react';
import {invoke} from '@tauri-apps/api/tauri';
import { useHotkeys } from 'react-hotkeys-hook';
import {v4 as uuidv4} from 'uuid';

interface ChildProps {
    addItem: Function
}

const MainEditor = (props: ChildProps) => {
    const [content, setContent] = useState('')

    useHotkeys('ctrl+enter', () => {
        saveData()
    })

    const editor = useEditor({
        extensions: [
            StarterKit,
            Placeholder.configure({
                placeholder: 'Typing here.'
            })
        ],
        content: '',
        onUpdate({ editor }) {
            setContent(editor.getHTML())
        }
    })

    const saveData = async () => {

        if(content.length <= 7) {
            return
        }

        props.addItem({
            id: uuidv4(),
            message: content,
            timestamp: new Date(),
        })

        const result = invoke('save_message', { invokeMessage: JSON.stringify({
            id: uuidv4(),
            message: content,
            timestamp: new Date(),
        }) })
        if(!result) {
            alert('error cannot save.')
        }

        editor?.commands.setContent("");
        setContent('')

    }

    return (
        <div className='editor-content'>
            <EditorContent editor={editor} className='editor-content-style' content={content}/>
            <div className='footer-editor'>
                <button onClick={() => saveData()}>Send</button>
            </div>
        </div>
    )
}

export default MainEditor